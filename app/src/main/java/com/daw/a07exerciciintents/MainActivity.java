package com.daw.a07exerciciintents;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText nom, cognom, phone, web;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nom = findViewById(R.id.edt_name);
        cognom = findViewById(R.id.edt_surname);
        web = findViewById(R.id.edt_web);
        phone = findViewById(R.id.edt_phone);
    }

    public void mostrar(View v) {

        if ( checkName(nom) && checkSurname(cognom) && checkWeb(web) && checkPhone(phone) ){
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra("nom", nom.getText().toString());
            intent.putExtra("cognom", cognom.getText().toString());
            intent.putExtra("web", web.getText().toString());
            intent.putExtra("telefon", phone.getText().toString());
            startActivity(intent);
        }
    }

    private boolean checkName(EditText nom) {
        String errorString=getString(R.string.error_nom);
        if ("".equals(nom.getText().toString())){
            nom.setError( errorString );
            return false;
        }
        return true;
    }

    private boolean checkSurname(EditText cognom) {
        String errorString=getString(R.string.error_surname);
        if ("".equals(cognom.getText().toString())){
            cognom.setError( errorString );
            return false;
        }
        return true;
    }

    private boolean checkWeb(EditText web) {
        String errorString=getString(R.string.error_web);
        if ("".equals(web.getText().toString())){
            web.setError( errorString );
            return false;
        }
        return true;
    }

    private boolean checkPhone(EditText phone) {
        String errorString=getString(R.string.error_phone);
        if ("".equals(phone.getText().toString())){
            phone.setError( errorString );
            return false;
        }
        return true;
    }

    public void deleteAll(View v) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle( getString(R.string.confirm_delete) );

        alertDialogBuilder.setMessage( getString(R.string.confirm_msg_delete) )
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        nom.setText("");
                        cognom.setText("");
                        web.setText("");
                        phone.setText("");
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}