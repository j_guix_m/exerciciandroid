package com.daw.a07exerciciintents;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    private String nom, cognom, web, phone;
    private TextView txt_name, txt_cognom, txt_web, txt_phone;
    private int counter;
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // SharedPreferences to save the number of times accessed
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        counter = prefs.getInt("counter", 0 );
        counter++;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("counter", counter );
        editor.commit();

        Toast.makeText(this, getString(R.string.access) + counter + getString(R.string.access_times) , Toast.LENGTH_LONG).show();


        // Sets data to the activity screen
        nom = getIntent().getStringExtra("nom");
        txt_name = findViewById(R.id.txt2_name);
        txt_name.setText(getString(R.string.name) + ": " + nom);

        cognom = getIntent().getStringExtra("cognom");
        txt_cognom = findViewById(R.id.txt2_name);
        txt_cognom.setText(getString(R.string.surname) + ": " + cognom);

        web = getIntent().getStringExtra("web");
        txt_web = findViewById(R.id.txt2_web);
        txt_web.setText(getString(R.string.web) + ": " + web);

        phone = getIntent().getStringExtra("telefon");
        txt_phone = findViewById(R.id.txt2_phone);
        txt_phone.setText(getString(R.string.phone) + ": " + phone);
    }

    public void buttonWeb(View view) {
        Intent intent = new Intent (Intent.ACTION_VIEW);
        intent.setData(Uri.parse( web ));
        startActivity(intent);
    }
    public void buttonCall(View view) {
        Intent intent = new Intent (Intent.ACTION_DIAL);
        intent.setData(Uri.parse( "tel:" + phone ));
        startActivity(intent);
    }
}